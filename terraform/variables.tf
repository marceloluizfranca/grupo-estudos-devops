variable "no_do_token" {
  default = "2xxc"
}

variable "region" {
  default = "nyc1"
}

variable "k8s_clustername" {
  default = "clusterddigitalocean"
}

variable "k8s_version" {
  default = "1.21.2-do.2"
}

variable "k8s_poolname" {
  default = "worker-pool"
}

variable "k8s_count" {
  default = "3"
}
