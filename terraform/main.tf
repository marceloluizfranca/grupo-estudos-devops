variable "do_token" {}

provider "digitalocean"{
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "kubernetes_cluster" {
  name    = var.k8s_clustername
  region  = var.region
  version = var.k8s_version

  tags = ["k8s"]

  # This default node pool is mandatory
  node_pool {
    name       = var.k8s_poolname
    size       = "s-2vcpu-2gb" 
    auto_scale = false
    node_count = var.k8s_count
    tags       = ["node-pool-tag"]
  }

}

output "cluster-id" {
  value = digitalocean_kubernetes_cluster.kubernetes_cluster.id
}

resource "local_file" "kubeconfigdo" {
  content  = digitalocean_kubernetes_cluster.kubernetes_cluster.kube_config.0.raw_config
  filename = "kubeconfig_do"
}

output "kubeconfig_path_do" {
  value = local_file.kubeconfigdo.filename
}
